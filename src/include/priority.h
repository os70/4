#ifndef PRIORITY_H
#define PRIORITY_H

#include "common.h"

std::pair<output, exec_chain> priority_preemt(input const &);
output priority(input const &);

#endif
