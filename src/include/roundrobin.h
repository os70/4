#ifndef RR_H
#define RR_H

#include "common.h"

std::pair<output, exec_chain> round_robin(input const &);

#endif
