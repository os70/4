#ifndef COMMON_H
#define COMMON_H

#include <deque>
#include <iostream>
#include <limits>

struct process {
  // boiler plate
  process() = default;
  process(process const &) = default;
  process(process &&) = default;
  process &operator=(process const &) = default;
  process &operator=(process &&) = default;

  process(std::istream &is);
  uint64_t arrival_time;
  uint64_t burst_time;
  uint64_t niceness;
  uint64_t pid;
  static uint64_t pid_counter;

  uint64_t completion_time;
  uint64_t turnaround_time;
  uint64_t response_time = std::numeric_limits<uint64_t>::max();
  uint64_t waiting_time;

  // member
  void print(std::ostream &os) const;
};

struct sys_clk {
  static uint64_t time;
};

// template<typename _T>
// struct ready_queue{
//   std::deque<_T>;
// };

using input = std::deque<process>;
using output = std::deque<process>;
using exec_chain = std::deque<std::pair<uint64_t, uint64_t>>;

#endif
