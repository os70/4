#ifndef SJFS_H
#define SJFS_H

#include "common.h"

std::pair<output, exec_chain> sjfs_preempt(input const &);
output sjfs(input const &);

#endif
