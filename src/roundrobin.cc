#include "roundrobin.h"
#include "config.h"
#include <algorithm>
#include <limits>

std::pair<output, exec_chain> round_robin(input const &in) {
  auto cpyIn = in;
  output out = cpyIn;
  out.push_back({});
  exec_chain ex;
  if (in.size() == 0)
    return {out, ex};
  std::sort(cpyIn.begin(), cpyIn.end(),
            [](decltype(cpyIn)::value_type const &a,
               decltype(cpyIn)::value_type const &b) {
              return a.arrival_time < b.arrival_time;
            });
  auto pickandrun = [&out, &ex, pos = 0, end = 0](input &in) mutable {
    while (end != in.size()) {
      if (in[end].arrival_time <= sys_clk::time) {
        ++end;
      } else {
        break;
      }
    }
    if (end == 0) {
      auto wait = in[0].arrival_time - sys_clk::time;
      ex.push_back({std::numeric_limits<uint64_t>::max(), wait});
      sys_clk::time += wait;
      out.back().pid = std::numeric_limits<decltype(out.back().pid)>::max();
      out.back().burst_time += wait;
      return;
    }
    pos %= end;
    uint64_t const time = (in[pos].burst_time >= __PREEMT_QUA)
                              ? __PREEMT_QUA
                              : in[pos].burst_time;
    ex.push_back({in[pos].pid, time});
    out[in[pos].pid].response_time =
        std::min(out[in[pos].pid].response_time, sys_clk::time);
    sys_clk::time += time;
    in[pos].burst_time -= time;
    if (in[pos].burst_time == 0) {
      auto &proc = out[in[pos].pid];
      proc.completion_time = sys_clk::time;
      proc.turnaround_time = proc.completion_time - proc.arrival_time;
      proc.waiting_time = proc.turnaround_time - proc.burst_time;
      in.erase(in.begin() + pos);
      --end;
    } else {
      ++pos;
    }
  };
  while (cpyIn.size() != 0) {
    pickandrun(cpyIn);
  }
  return {out, ex};
}
