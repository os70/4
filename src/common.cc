#include "common.h"

uint64_t process::pid_counter = 0;
uint64_t sys_clk::time = 0;

process::process(std::istream &is) {
  is >> arrival_time >> burst_time >> niceness;
  pid = pid_counter++;
}

void process::print(std::ostream &os) const {
  os << pid << " " << arrival_time << " " << burst_time << " " << niceness
     << " " << completion_time << " " << turnaround_time << " " << response_time
     << " " << waiting_time;
}
