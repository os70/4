#include "sjfs.h"
#include "config.h"
#include <algorithm>

output sjfs(input const &in) {
  static auto pickandrun = [](input &in, output &out) {
    auto iter = in.begin();
    for (; iter != in.end(); ++iter) {
      if (iter->arrival_time <= sys_clk::time)
        break;
    }
    if (iter == in.end()) {
      ++sys_clk::time;
      return;
    }
    out.push_back(*iter);
    auto &comp_process = out.back();
    comp_process.response_time = sys_clk::time - comp_process.arrival_time;
    sys_clk::time += iter->burst_time;
    comp_process.completion_time = sys_clk::time;
    comp_process.turnaround_time =
        comp_process.completion_time - comp_process.arrival_time;
    comp_process.waiting_time =
        comp_process.turnaround_time - comp_process.burst_time;
    in.erase(iter);
  };

  auto cpyIn = in;
  std::stable_sort(cpyIn.begin(), cpyIn.end(),
                   [](decltype(cpyIn)::value_type const &a,
                      decltype(cpyIn)::value_type const &b) {
                     return a.arrival_time < b.arrival_time;
                   });
  std::stable_sort(cpyIn.begin(), cpyIn.end(),
                   [](decltype(cpyIn)::value_type const &a,
                      decltype(cpyIn)::value_type const &b) {
                     return a.burst_time < b.burst_time;
                   });
  output out;
  while (cpyIn.size() != 0)
    pickandrun(cpyIn, out);
  return out;
}

std::pair<output, exec_chain> sjfs_preempt(input const &in) {
  auto cpyIn = in;
  auto out = in;
  exec_chain ex;
  auto pickandrun =
      [&out, &ex](input &in,
                  std::remove_reference_t<decltype(in)>::iterator &old) {
        auto iter = in.begin();
        while (iter != in.end()) {
          if (iter->arrival_time <= sys_clk::time) {
            iter = (iter->burst_time < old->burst_time) ? iter : old;
            break;
          }
          ++iter;
        }
        //for waiting
        if (iter == in.end()) {
          std::remove_reference_t<decltype(in)>::iterator min = in.begin();
          for (int i = 0; i < in.size(); ++i) {
            if (min->arrival_time > in[i].arrival_time) {
              min = in.begin() + i;
            }
          }
          auto wait = min->arrival_time - sys_clk::time;
          sys_clk::time += (wait);
          ex.push_back(
              {std::numeric_limits<decltype(ex)::value_type::first_type>::max(),
               wait});
          return iter - 1;
        }
        // end waiting
        auto &proc = out[iter->pid];
        auto time = (iter->burst_time <= __PREEMT_QUA) ? iter->burst_time
                                                       : __PREEMT_QUA;
        if (proc.response_time >= sys_clk::time) {
          proc.response_time = sys_clk::time;
        }
        iter->burst_time -= time;
        ex.push_back({iter->pid, time});
        sys_clk::time += time;
        if (iter->burst_time == 0) {
          proc.completion_time = sys_clk::time;
          proc.turnaround_time = proc.completion_time - proc.arrival_time;
          proc.waiting_time = proc.turnaround_time - proc.burst_time;
          in.erase(iter);
          return in.end() - 1;
        }
        return iter;
      };
  std::stable_sort(cpyIn.begin(), cpyIn.end(),
                   [](decltype(cpyIn)::value_type const &a,
                      decltype(cpyIn)::value_type const &b) {
                     return a.arrival_time < b.arrival_time;
                   });
  std::stable_sort(cpyIn.begin(), cpyIn.end(),
                   [](decltype(cpyIn)::value_type const &a,
                      decltype(cpyIn)::value_type const &b) {
                     return a.burst_time < b.burst_time;
                   });
  auto iter = cpyIn.end() - 1;
  while (cpyIn.size() != 0)
    iter = pickandrun(cpyIn, iter);
  return {out, ex};
}
