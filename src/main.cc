#include "fcfs.h"
#include "priority.h"
#include "roundrobin.h"
#include "sjfs.h"
input getInput() {
  input in;
  int temp;
  std::cin >> temp;
  for (int i = 0; i < temp; ++i)
    in.emplace_back(std::cin);
  return in;
}

int main() {
  auto in = getInput();
  std::cout << "id \tat \tbt \tnc \tct \ttt \trt \twt \n";
  std::cout << "SJF --- \n";
  sys_clk::time = 0;
  auto out = sjfs(in);
  uint64_t avg_tat = 0;
  uint64_t avg_wait = 0;
  for (auto const &iter : out) {
    avg_wait += iter.waiting_time;
    avg_tat += iter.turnaround_time;
    iter.print(std::cout);
    std::cout << "\n";
  }
  std::cout << "Avg Waiting Time: " << avg_wait / out.size()
            << " Avg Turnaround Time: " << avg_tat / out.size() << "\n";
  std::cout << "------\n";
  std::cout << "Priority \n";
  sys_clk::time = 0;
  out = priority(in);
  avg_tat = 0;
  avg_wait = 0;
  for (auto const &iter : out) {
    avg_wait += iter.waiting_time;
    avg_tat += iter.turnaround_time;
    iter.print(std::cout);
    std::cout << "\n";
  }
  std::cout << "Avg Waiting Time: " << avg_wait / out.size()
            << " Avg Turnaround Time: " << avg_tat / out.size() << "\n";
  std::cout << "---------\n";
  sys_clk::time = 0;
  std::cout << "FCFS\n";
  out = fcfs(in);
  avg_tat = 0;
  avg_wait = 0;
  for (auto const &iter : out) {
    avg_wait += iter.waiting_time;
    avg_tat += iter.turnaround_time;
    iter.print(std::cout);
    std::cout << "\n";
  }
  std::cout << "Avg Waiting Time: " << avg_wait / out.size()
            << " Avg Turnaround Time: " << avg_tat / out.size() << "\n";
  std::cout << "---------\n";
  sys_clk::time = 0;
  std::cout << "priority_preemt\n";
  {
    auto [out, ex] = priority_preemt(in);
    avg_tat = 0;
    avg_wait = 0;
    for (auto const &iter : out) {
      avg_wait += iter.waiting_time;
      avg_tat += iter.turnaround_time;
      iter.print(std::cout);
      std::cout << "\n";
    }
    std::cout << "Avg Waiting Time: " << avg_wait / out.size()
              << " Avg Turnaround Time: " << avg_tat / out.size() << "\n";
  }
  std::cout << "---------\n";
  sys_clk::time = 0;
  std::cout << "sjfs_preemt\n";
  {
    auto [out, ex] = sjfs_preempt(in);
    avg_tat = 0;
    avg_wait = 0;
    std::cout << ex.front().first << ex.front().second << "\n";
    for (auto const &iter : out) {
      avg_wait += iter.waiting_time;
      avg_tat += iter.turnaround_time;
      iter.print(std::cout);
      std::cout << "\n";
    }
  }
  std::cout << "Avg Waiting Time: " << avg_wait / out.size()
            << " Avg Turnaround Time: " << avg_tat / out.size() << "\n";
  std::cout << "---------\n";
  sys_clk::time = 0;
  std::cout << "round_robin\n";
  {
    auto [out, ex] = round_robin(in);
    avg_tat = 0;
    avg_wait = 0;
    for (auto const &iter : out) {
      avg_wait += iter.waiting_time;
      avg_tat += iter.turnaround_time;
      iter.print(std::cout);
      std::cout << "\n";
    }
    std::cout << "Avg Waiting Time: " << avg_wait / out.size()
              << " Avg Turnaround Time: " << avg_tat / out.size() << "\n";
  }
}
