#include "fcfs.h"
#include <algorithm>
#include <iostream>
#include <type_traits>
#include <vector>

bool cmp(input const &, input const &);

output fcfs(input const &in) {
  auto cpyIn = in;
  std::sort(cpyIn.begin(), cpyIn.end(),
            [](decltype(cpyIn)::value_type const &a,
               decltype(cpyIn)::value_type const &b) {
              return a.arrival_time < b.arrival_time;
            });

  output outputs;

  for (auto const &iter : cpyIn) {
    outputs.push_back(iter);
    auto &comp_process = outputs.back();
    if (comp_process.arrival_time > sys_clk::time) {
      sys_clk::time = comp_process.arrival_time;
    }
    comp_process.response_time = sys_clk::time - comp_process.arrival_time;
    sys_clk::time += comp_process.burst_time;
    comp_process.completion_time = sys_clk::time;
    comp_process.turnaround_time =
        comp_process.completion_time - comp_process.arrival_time;
    comp_process.waiting_time =
        comp_process.turnaround_time - comp_process.burst_time;
  }
  return outputs;
}
